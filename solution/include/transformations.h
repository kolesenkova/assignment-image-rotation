#ifndef TRANSFORMATIONS_H
#define TRANSFORMATIONS_H
#include "../include/structures.h"
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height);
struct image rotate_counterclock_wise_90(const struct image img);
struct pixel* adress_by_index(const struct image img, size_t i, size_t j);

#endif //TRANSFORMATIONS_H

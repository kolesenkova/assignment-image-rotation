#ifndef UTIL_H
#define UTIL_H
#include "../include/file_io.h"
#include <stdio.h>

// bool open_files(FILE** input_file, FILE** output_file, char* input_filename, char* output_filename);
bool close_files(FILE** input_file, FILE** output_file);

#endif //UTIL_H

#ifndef STRUCTURES_H
#define STRUCTURES_H
#include <stdint.h>

//#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
//#pragma pack(pop)

struct pixel {
    uint8_t b, g, r;
};

#endif //STRUCTURES_H

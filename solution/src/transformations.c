#include "../include/transformations.h"
#include "../include/memory.h"
// #include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct image new_img;
    new_img.width = width;
    new_img.height = height;
    pixels_malloc_memory(&new_img.data, width, height);
    return new_img;
}

struct pixel* adress_by_index(const struct image img, size_t i, size_t j) {
    return img.data + i * img.width + j;
}

struct image rotate_counterclock_wise_90(const struct image img){
    struct image rotated_img = image_create(img.height, img.width);
    for (size_t i = 0; i < rotated_img.height; i++) {
        for (size_t j = 0; j < rotated_img.width; j++) {
            *(adress_by_index(rotated_img, i, j)) = *(adress_by_index(img, img.height - j - 1, i));
        }
    }

    return rotated_img;
}

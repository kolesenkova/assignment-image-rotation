#include "../include/util.h"

/*bool open_files(FILE** input_file, FILE** output_file, char* input_filename, char* output_filename) {
    bool input_file_opened, output_file_opened;
    input_file_opened = !open_file(input_file, input_filename, "rb");
    output_file_opened = !open_file(output_file, output_filename, "wb");
    if (input_file_opened) {
        if (!output_file_opened) {
            close_file(output_file);
        }
        return 1;
    }
    if (output_file_opened) {
        fprintf(stderr, "Cannot create new .bmp file\n");
        if (!input_file_opened) {
            close_file(input_file);
        }
        return 1;
    }
    return 0;
}*/

bool close_files(FILE** input_file, FILE** output_file) {
    return (bool) (fclose(*input_file) || fclose(*output_file));
}

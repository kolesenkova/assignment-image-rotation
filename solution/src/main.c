#include "../include/bmp_io.h"
#include "../include/file_io.h"
#include "../include/transformations.h"
#include "../include/util.h"
#include <stdio.h>

int main(int argc, char** argv) {
    char* input_filename = NULL;
    char* output_filename = NULL;
    struct image img;
    struct image rotated_image;
    FILE* input_file = NULL;
    FILE* output_file = NULL;

    // Reading command line arguments
    if (argc < 3) {
        fprintf(stderr, "Not enough command line arguments\n");
        return -1;
    }
    input_filename = argv[1];
    output_filename= argv[2];
    
    // Opening files and checking if it were successful
    bool input_file_opened, output_file_opened;
    input_file_opened = open_file(&input_file, input_filename, "rb");
    output_file_opened = open_file(&output_file, output_filename, "wb");
    if (!input_file_opened) {
        if (!output_file_opened) {
            fprintf(stderr, "Cannot open output file\n");
        }
        else {
            close_file(&output_file);
        }
        fprintf(stderr, "Cannot open input file\n");
        return -1;
    }
    else {
        if (!output_file_opened) {
            close_file(&input_file);
            fprintf(stderr, "Cannot open output file\n");
            return -1;
        }
    }
    fprintf(stdout, "Files were successfully opened\n");

    // Reading input file as .bmp
    if (from_bmp(input_file, &img)) {
        fprintf(stderr, "Cannot read file as .bmp file, may be its not actually a .bmp file\n");
        return -1;
    }
    fprintf(stdout, "File was successfully read\n");

    // Rotating image by 90 degree counterclock-wise
    rotated_image = rotate_counterclock_wise_90(img);

    // Writing rotated image in output file
    if (to_bmp(output_file, &rotated_image)) {
        fprintf(stderr, "Cannot convert image into .bmp\n");
        pixels_free_memory(rotated_image.data);
        pixels_free_memory(img.data);
        if (close_files(&input_file, &output_file)) {
            fprintf(stderr, "Cannot close files\n");
        }
        return -1;
    }
    fprintf(stdout, "Image was successfully converted into .bmp\n");

    // Freeing memory and closing input and output files
    pixels_free_memory(rotated_image.data);
    pixels_free_memory(img.data);
    if (close_files(&input_file, &output_file)) {
        fprintf(stderr, "Cannot close files\n");
        return -1;
    }
    fprintf(stdout, "Files were successfully closed\n");
    
    return 0;
}
